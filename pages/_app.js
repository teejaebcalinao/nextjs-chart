import 'bootstrap/dist/css/bootstrap.min.css' 
import Navibar from '../components/Navibar'
import {Container} from 'react-bootstrap'

export default function App({Component, pageProps}) {


	return (
		<React.Fragment>
			<Navibar />
			<Container>
				<Component {...pageProps}/>
			</Container>
		</React.Fragment>

		)

}